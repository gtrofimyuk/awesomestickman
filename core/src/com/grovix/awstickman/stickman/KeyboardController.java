package com.grovix.awstickman.stickman;

import com.badlogic.gdx.Input.Buttons;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputAdapter;

//input Multiplexer in level Screen
public class KeyboardController extends InputAdapter {
	
	private StickmanAction action;
	
	public KeyboardController(StickmanAction inputAction){
		action = inputAction;
	}
	
	public boolean touchDown(int screenX, int screenY, int pointer,
			int button) {
		switch(button){
		case Buttons.RIGHT:
			action.getCursorDirection();
			action.armKick();
		break;
		case Buttons.LEFT:
			action.getCursorDirection();
			action.legKick();
		break;
		}
		return true;
	}
	
	
	
	public boolean keyDown(int keycode) {
		switch(keycode){
		case Keys.Q:
			action.twist(5000); //TODO: send it to ForceConst
			break;
		case Keys.E:
			action.twist(-5000);
			break;
		case Keys.W:
			action.upMove();
			break;
		case Keys.D:
			action.rightMove();
			break;
		case Keys.A:
			action.leftMove();
			break;
		case Keys.S:
			action.downMove();
			break;
		case Keys.SPACE:
			action.deceleration();
		break;
		case Keys.SHIFT_LEFT:
			action.getCursorDirection();
			action.legKick();
		break;
		}
		
		return true;
	}
	
	public boolean keyUp(int keycode) {
		switch(keycode){	
		case Keys.A:
			action.stopLeft();
			action.deceleration();
			break;
		case Keys.D:
			action.stopRight();
			action.deceleration();
			break;
		case Keys.W:
			action.stopUp();
			action.deceleration();
			break;
		case Keys.S:
			action.stopDown();
			action.deceleration();
			break;
			//action.stopMoving();
			//action.deceleration();
		}
		
		return true;
	}

}