package com.grovix.awstickman.stickman;

import com.badlogic.gdx.math.Vector2;

public class StickmanState {
	
	private float health;
	private float maxHealth;
	
	public Vector2 throwVector;
	private boolean isThrowed;
	public float throwTime = 0;
	
	public StickmanState(float inputHealth) {
		health = inputHealth;
		maxHealth = inputHealth;
	}

	public float getHealth() {
		return health;
	}
	public void setHealth(float health) {
		this.health = health;
	}
	
	public void increaseHealth(float increase){
		health +=increase;
		if(health > maxHealth)
			health = maxHealth;
	}
	
	public void decreaseHealth(float decrease){
		if(health > -0.1f)
		health -= decrease;
	}
	
	public boolean isThrowed(){
		return isThrowed;
	}
	
	public void throwComlete(){
		isThrowed = false;
		throwTime = 0;
	}
	
	public void throwBody(Vector2 inputVector, float throwTime){
		throwVector = inputVector;
		isThrowed = true;
		this.throwTime = throwTime;
	}
	
}
