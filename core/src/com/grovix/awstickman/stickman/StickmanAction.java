package com.grovix.awstickman.stickman;

import java.util.Map;
import java.util.TreeMap;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.ContactFilter;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.grovix.awstickman.config.ForceConst;
import com.grovix.awstickman.config.StickmanConst;
import com.grovix.awstickman.config.WorldConst;
import com.grovix.awstickman.factory.stickman.Parts;
import com.grovix.awstickman.factory.stickman.StickmanBody;


public class StickmanAction implements ContactFilter{
	
	public Map<Integer, Body> bodyMap = new TreeMap<>();
	private StickmanBody b;
	public StickmanConst c;
	
	private WorldConst w = new WorldConst();
	public ForceConst f;
	
	private float m[] = new float[10];
	private float sumM = 0;
	
	public boolean isFly = true;
	public boolean isRightMove = false;
	public boolean isLeftMove = false;
	public boolean isUpMove = false;
	public boolean isDownMove = false;
	private boolean isLegKick = false;
	public boolean isArmKick = false;
	public boolean isLive = true;
	
	private Vector2 direction = new Vector2(0,0);
	
	int timerLeg = 0;
	int timerArm = 0;
	float kickDelay = 0;
	float armKickDelay = 0;
		
	public StickmanAction(StickmanBody body, ForceConst force) {
		b = body;
		c = body.c;
		f = force;
		//creating a map
		bodyMap.put(Parts.HEAD, b.head);
		bodyMap.put(Parts.TORSO, b.torso);
		bodyMap.put(Parts.LEFT_SHOULDER, b.leftShoulder);
		bodyMap.put(Parts.RIGHT_SHOULDER, b.rightShoulder);
		bodyMap.put(Parts.LEFT_THIGH, b.leftThigh);
		bodyMap.put(Parts.RIGHT_THIGH, b.rightThigh);
		bodyMap.put(Parts.LEFT_FOREARM, b.leftForearm);
		bodyMap.put(Parts.RIGHT_FOREARM, b.rightForearm);
		bodyMap.put(Parts.LEFT_SHANK, b.leftShank);
		bodyMap.put(Parts.RIGHT_SHANK, b.rightShank);
		for(int i=0; i < 10; i++){
			m[i]=bodyMap.get(i).getMass();
			sumM += m[i];
		}
		
		for(Body elem : bodyMap.values()){
			elem.setBullet(false);
		}
		
	}
	
	public void update(){
		this.setFly(isFly);
		if(kickDelay > 0)
			kickDelay -= 0.5f/35; //TODO : move this to config
		if(armKickDelay > 0)
			armKickDelay -=0.5f/5;
		if(isRightMove)
			this.simpleRightMove();
		if(isLeftMove)
			this.simpleLeftMove();	
		if(isUpMove)
			this.simpleUpMove();
		if(isDownMove)
			this.simpleDownMove();
		if(isLegKick){
			timerLeg+=1;
		}
		if(isArmKick){
			timerArm+=1;
		}
		if(timerLeg > 75){ //TODO : send this value to config
			timerLeg = 0;
			isLegKick = false;
			b.torso.setAngularDamping(0);
		}
		
		if(timerArm > 25){
			timerArm = 0;
			isArmKick = false;
		}
			
	}
	
	public boolean isKick(){
		return isLegKick;
	}

	public void rightMove(){
		isRightMove = true;
	}
	
	public void leftMove(){
		isLeftMove = true;
	}
	
	public void upMove(){
		isUpMove = true;
	}
	
	public void downMove(){
		isDownMove = true;
	}
	
	public void stopUp(){
		isUpMove = false;
	}
	
	public void stopDown(){
		isDownMove = false;
	}
	
	public void stopLeft(){
		isLeftMove = false;
	}
	
	public void stopRight(){
		isRightMove = false;
	}
	
	public void stopMoving(){
		isRightMove = false;
		isLeftMove = false;
		isUpMove = false;
		isDownMove = false;
	}
	
	public void stopKick(){
		isLegKick = false;
	}
	
	public void deceleration(){
		for(Body elem : bodyMap.values()){
			float angVelocity = elem.getAngularVelocity();
			elem.applyLinearImpulse(-elem.getLinearVelocity().x*elem.getMass() * f.deceleration, 
									-elem.getLinearVelocity().y*elem.getMass() * f.deceleration,
									 elem.getWorldCenter().x, 
									 elem.getWorldCenter().y, true);
			elem.applyAngularImpulse(-angVelocity * elem.getMass() * f.deceleration , true);
		}
	}
	
	public void twist(int impulse){
		b.torso.applyAngularImpulse(-impulse, true);
	}

	@Override
	public boolean shouldCollide(Fixture fixtureA, Fixture fixtureB) {
		if((Integer)fixtureA.getBody().getUserData()/10 == (Integer) fixtureB.getBody().getUserData()/10)
			return false;
		return true;
	}

	public void setFly(boolean flag){
		if(flag){
				b.torso.applyForceToCenter(0, sumM*w.g*(-1f), true);
		}
	}
	
	public void legKick(){
		if(kickDelay < 0.01f){
			isLegKick = true;
			kickDelay = 0.1f;
			timerLeg = 0;
			b.torso.setAngularDamping(10000);
			if(direction.x > 0)
				rightLegKick(direction);
			else
				leftLegKick(direction);
		//aka.ms/wpstudcamp
		}
	}
	
	public void armKick(){
		if(armKickDelay < 0.01f){
			getCursorDirection();
			isArmKick = true;
			if(direction.x > 0)
				b.rightForearm.applyForceToCenter(f.legForce * direction.x , f.legForce * direction.y, true);
			else
				b.leftForearm.applyForceToCenter(f.legForce * direction.x, f.legForce * direction.y, true);
		}
	}
	
	public void getCursorDirection(){
		Vector2 cursorCord =  new Vector2((Gdx.input.getX() - Gdx.graphics.getWidth()/2),
				-(Gdx.input.getY() - Gdx.graphics.getHeight()/2));
		float norma = (float) Math.sqrt(Math.pow((cursorCord.x), 2) + Math.pow(cursorCord.y, 2));
		direction.set(cursorCord.x/norma, cursorCord.y/norma);
		}
	
	public Vector2 getDirection(){
		return direction;
	}
	
	public void setDirection(Vector2 inputDirection){
		direction = inputDirection;
	}
	
	private void leftLegKick(Vector2 direction){
		b.leftThigh.applyForceToCenter(f.legForce * direction.x, f.legForce*direction.y, true);
		b.leftShank.applyForceToCenter(f.legForce * direction.x, f.legForce*direction.y, true);
	}
	
	private void rightLegKick(Vector2 direction){
		b.rightThigh.applyForceToCenter(f.legForce * direction.x, f.legForce*direction.y, true);
		b.rightShank.applyForceToCenter(f.legForce * direction.x, f.legForce*direction.y, true);
	}
	
	public void simpleRightMove(){
		b.rightForearm.applyForceToCenter(sumM*w.g*(-f.moveForce), 0, true);
		b.leftForearm.applyForceToCenter(sumM*w.g*(-f.moveForce), 0, true);
	}
	
	public void simpleLeftMove(){
		b.rightForearm.applyForceToCenter(sumM*w.g*(f.moveForce), 0, true);
		b.leftForearm.applyForceToCenter(sumM*w.g*(f.moveForce), 0, true);

	}
	
	public void simpleUpMove(){
		b.rightForearm.applyForceToCenter(0, sumM*w.g*(-f.moveForce), true);
		b.leftForearm.applyForceToCenter(0, sumM*w.g*(-f.moveForce), true);

	}
	
	public void simpleDownMove(){
		b.rightForearm.applyForceToCenter(0, sumM*w.g*(f.moveForce), true);
		b.leftForearm.applyForceToCenter(0, sumM*w.g*(f.moveForce), true);
	}
	
	//� �������� �������� ����
	public Vector2 getPosition(){
		return b.torso.getPosition();
	}
	
	public void death(){
		setActive(false);
	}
	
	public void setActive(boolean isActive){
		for(Body elem: bodyMap.values()){
			elem.setActive(isActive);
		}
	}	
}
