package com.grovix.awstickman.stickman;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.grovix.awstickman.config.ForceConst;
import com.grovix.awstickman.config.StickmanConst;
import com.grovix.awstickman.factory.stickman.StickmanBody;


public class Stickman {
	
	public StickmanBody body;
	public StickmanState state;
	public StickmanAction action;
	
	public ForceConst force;
	public boolean isLive = true;
	public boolean isThrowTimeSet = false;
	
	public float throwTimer;
	public int deathTimer = 300; //Make it config
	
	public Stickman(World importWorld, Vector2 position, StickmanConst inputC, ForceConst inputForce){
		force = inputForce;
		body = new StickmanBody(importWorld, position, inputC);
		state = new StickmanState(inputC.health);
		action = new StickmanAction(body, force);
		
		importWorld.setContactFilter(action);
		
	}
	
	public Vector2 getPosition(){
		return body.torso.getPosition();
	}
	
	public float getVelocity(){
		return (float) Math.sqrt(Math.pow(body.torso.getLinearVelocity().x,2) + Math.pow(body.torso.getLinearVelocity().y,2));
	}
	
	public void update(){
		action.update();
		if(this.state.getHealth() < 0){
			this.death();
		}
		if(state.isThrowed()){
			if(!isThrowTimeSet){
				throwTimer = state.throwTime;
				isThrowTimeSet = true;
			}
			else if(throwTimer > 0){
				body.torso.applyForceToCenter(state.throwVector, true);
				throwTimer -= 1f;
			}
			else{
				state.throwComlete();
				throwTimer = 0;
				isThrowTimeSet = false;
			}
		}
	}
	
	public int getId(){
		return (Integer)body.torso.getUserData();
	}
	
	public Body getPart(int partId){
		return action.bodyMap.get(partId);
	}
	
	public void death(){
		action.setFly(false);
		isLive = false;
		if(deathTimer == 1)
			action.death();
	}

}
