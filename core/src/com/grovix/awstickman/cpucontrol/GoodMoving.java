package com.grovix.awstickman.cpucontrol;

import com.badlogic.gdx.math.Vector2;
import com.grovix.awstickman.kernel.HeroPositionGetter;
import com.grovix.awstickman.stickman.StickmanAction;

public class GoodMoving extends Controller{

	private Vector2 temp = new Vector2(-1,-1);

	public GoodMoving(HeroPositionGetter inputKernel,
			StickmanAction inputAction) {
		super(inputKernel, inputAction);
	}

	@Override
	public void update() {
		
		Vector2 move= new Vector2(getHeroPosition().x - action.getPosition().x, getHeroPosition().y - action.getPosition().y); 
		if(temp.x/Math.abs(temp.x) != move.x/Math.abs(move.x) &
				(Math.abs(temp.x + move.x) > 0.1)){ // TODO: make it const
			action.deceleration();
		}
		
		if(temp.y/Math.abs(temp.y) != move.y/Math.abs(move.y) &
				(Math.abs(temp.y + move.y) > 0.1)){
			action.deceleration();
		}
		
		if(move.x < 0){
			action.simpleLeftMove();
		}
		else{
			action.simpleRightMove();
		}
		if(move.y < 0){
			action.simpleDownMove();
		}
		else{
			action.simpleUpMove();
		}
		
		temp = move;
		
		if(Math.sqrt((move.x)*(move.x) + (move.y)*(move.y)) < c.legY*4){
			action.setDirection(move);
			action.legKick();
		}
		if(Math.sqrt((move.x)*(move.x) + (move.y)*(move.y)) < c.armY*2f)
			action.armKick();
	}
}
