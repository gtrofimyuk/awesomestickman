package com.grovix.awstickman.cpucontrol;

import com.badlogic.gdx.math.Vector2;
import com.grovix.awstickman.config.StickmanConst;
import com.grovix.awstickman.kernel.HeroPositionGetter;
import com.grovix.awstickman.stickman.StickmanAction;

public abstract class Controller {
	protected StickmanAction action;
	protected HeroPositionGetter kernel;
	protected StickmanConst c;
	
	public Controller(HeroPositionGetter inputKernel, StickmanAction inputAction) {
		kernel = inputKernel;
		action = inputAction;
		c = inputAction.c;
	}
	
	public Vector2 getHeroPosition(){
		return kernel.getHeroPosition();
	}
	
	public abstract void update();

}
