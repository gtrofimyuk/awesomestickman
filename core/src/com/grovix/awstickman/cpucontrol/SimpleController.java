package com.grovix.awstickman.cpucontrol;

import com.badlogic.gdx.math.Vector2;
import com.grovix.awstickman.kernel.HeroPositionGetter;
import com.grovix.awstickman.stickman.StickmanAction;

public class SimpleController extends Controller{
	
	public SimpleController(HeroPositionGetter inputKernel, StickmanAction inputAction) {
		super(inputKernel, inputAction);
	}
	
	public void update(){

		Vector2 move= new Vector2(getHeroPosition().x - action.getPosition().x, getHeroPosition().y - action.getPosition().y); 
		if(move.x < 0){
			action.simpleLeftMove();
		}
		else{
			action.simpleRightMove();
		}
		if(move.y < 0){
			action.simpleDownMove();
		}
		else{
			action.simpleUpMove();
		}
		if(Math.sqrt((move.x)*(move.x) + (move.y)*(move.y)) < c.legY*4){
			action.setDirection(move);
			action.legKick();
		}
		if(Math.sqrt((move.x)*(move.x) + (move.y)*(move.y)) < c.armY*2f)
			action.armKick();

	}
}
