package com.grovix.awstickman.factory.stickman;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.grovix.awstickman.config.StickmanConst;


//don't forget about dispose!

//are we need to save Joints variables?
public class StickmanBody{

	public static int id = 10;
	
	public Body head, torso, rightShoulder, leftShoulder, rightForearm, 
				 leftForearm, rightThigh, leftThigh, rightShank, leftShank;
	public StickmanConst c;
	
	private BodyDef bodyDef;
	private StickmanFixture fixture;
	private World world;
	
	private float pi = (float) Math.PI;
	private float rja = 0.95f;
	private int incrementCount = 1;
	
	public StickmanBody(World importWorld, Vector2 position, StickmanConst inputC){
		
		c = inputC;
		
		bodyDef= new BodyDef();
		fixture = new StickmanFixture(c);
		
		bodyDef.position.set(position);
		bodyDef.type = BodyDef.BodyType.DynamicBody;
		
		world = importWorld;
		
		torso = world.createBody(bodyDef);
		torso.setUserData(id); 
		torso.createFixture(fixture.bodyFixtureDef);
		System.out.println(id);
		incrementId();
		
		bodyDef.position.set(position.x, position.y+c.headRadius);
		head = world.createBody(bodyDef);
		head.setUserData(id);
		
		incrementId();
		
		head.createFixture(fixture.headFixtureDef);
		
		bodyDef.position.set(position.x-c.bodyX, position.y - c.bodyY-3*c.legY/2);
		leftThigh = world.createBody(bodyDef);
		leftThigh.setUserData(id); 
		leftThigh.createFixture(fixture.legFixtureDef);
		
		incrementId();
		
		bodyDef.position.set(position.x + c.bodyX, position.y - c.bodyY-3*c.legY/2);
		rightThigh = world.createBody(bodyDef);
		rightThigh.setUserData(id);
		rightThigh.createFixture(fixture.legFixtureDef);
		
		incrementId();
		
		bodyDef.position.set(position.x - c.bodyX, position.y - c.bodyY-4*c.legY);
		leftShank = world.createBody(bodyDef);
		leftShank.setUserData(id); 
		leftShank.createFixture(fixture.legFixtureDef);
		
		incrementId();
		
		bodyDef.position.set(position.x + c.bodyX, position.y - c.bodyY-4*c.legY);
		rightShank = world.createBody(bodyDef);
		rightShank.setUserData(id);
		rightShank.createFixture(fixture.legFixtureDef);
		
		incrementId();
		
		bodyDef.position.set(position.x - c.bodyX, position.y + c.bodyY);
		leftShoulder = world.createBody(bodyDef);
		leftShoulder.setUserData(id);
		leftShoulder.createFixture(fixture.armFixtureDef);

		incrementId();
		
		bodyDef.position.set(position.x + c.bodyX, position.y + c.bodyY);
		rightShoulder = world.createBody(bodyDef);
		rightShoulder.setUserData(id);
		rightShoulder.createFixture(fixture.armFixtureDef);
		
		incrementId();

		bodyDef.position.set(position.x - c.bodyX, position.y +c.bodyY - c.armX/2);
		leftForearm = world.createBody(bodyDef);
		leftForearm.setUserData(id); 
		leftForearm.createFixture(fixture.armFixtureDef);

		incrementId();
		
		bodyDef.position.set(position.x + c.bodyX, position.y +c.bodyY - c.armX/2);
		rightForearm = world.createBody(bodyDef);
		rightForearm.setUserData(id);
		rightForearm.createFixture(fixture.armFixtureDef);
		
		incrementId();
		//join head to torso
		DistanceJointDef distanceJointDef = new DistanceJointDef();
		
		distanceJointDef.bodyA = head;
		distanceJointDef.bodyB = torso;
		distanceJointDef.localAnchorA.set(head.getLocalCenter().x, head.getLocalCenter().y - c.headRadius);
		distanceJointDef.localAnchorB.set(torso.getLocalCenter().x, torso.getLocalCenter().y + c.bodyY);
		distanceJointDef.length = 0f;
		world.createJoint(distanceJointDef);
		
		//join thighs to torso
		RevoluteJointDef join = new RevoluteJointDef();
		join.bodyA = torso;
		join.bodyB = leftThigh;
		join.localAnchorB.set(leftThigh.getLocalCenter().x , leftThigh.getLocalCenter().y + c.legY*rja);
		join.localAnchorA.set(torso.getLocalCenter().x - c.bodyX*c.joinTorso, torso.getLocalCenter().y - c.bodyY);
		join.lowerAngle = -1.3f*pi/2;
		join.upperAngle = pi/12;
		join.enableLimit = true;

		world.createJoint(join);
		
		join.lowerAngle =  -pi/12;
		join.upperAngle = 1.3f*pi/2;
		join.localAnchorA.set(torso.getLocalCenter().x + c.bodyX*c.joinTorso, torso.getLocalCenter().y - c.bodyY);
		join.bodyB = rightThigh;
		join.lowerAngle = pi/12; 
		join.upperAngle = 1.1f*pi/2;
		world.createJoint(join);
		
		//join shoulders to torso

		join.localAnchorA.set(torso.getLocalCenter().x - c.bodyX*c.joinTorso, torso.getLocalCenter().y + c.bodyY);
		join.bodyB = leftShoulder;
		join.localAnchorB.set(leftShoulder.getLocalCenter().x , leftShoulder.getLocalCenter().y + c.armY*rja);
		join.enableLimit =false;
		world.createJoint(join);
		
		join.localAnchorA.set(torso.getLocalCenter().x + c.bodyX*c.joinTorso, torso.getLocalCenter().y + c.bodyY);
		join.bodyB = rightShoulder;
		world.createJoint(join);
		
		//join forearms to shoulders
		//join.enableLimit =true;
		join.bodyA = leftShoulder;
		join.bodyB = leftForearm;
		join.localAnchorA.set(leftShoulder.getLocalCenter().x, leftShoulder.getLocalCenter().y - c.armY*rja);
		join.localAnchorB.set(leftForearm.getLocalCenter().x , leftForearm.getLocalCenter().y + c.armY*rja);
		join.lowerAngle = 0; 
		join.upperAngle = 1.5f*pi/2;
		world.createJoint(join);
		
		join.bodyA = rightShoulder;
		join.bodyB = rightForearm;
		join.lowerAngle = -2.5f*pi/2;
		join.upperAngle = 0;
		world.createJoint(join);
		
		//join shanks to thigh
		join.enableLimit = false;
		join.bodyA = leftThigh;
		join.bodyB = leftShank;
		join.localAnchorA.set(leftThigh.getLocalCenter().x, leftThigh.getLocalCenter().y - c.legY*rja);
		join.localAnchorB.set(leftShank.getLocalCenter().x , leftShank.getLocalCenter().y + c.legY*rja);
		world.createJoint(join);
		
		join.bodyA = rightThigh;
		join.bodyB = rightShank;
		world.createJoint(join);
		
		fixture.polygons.dispose();
		
	}
	
	private void incrementId(){

		StickmanBody.id += incrementCount;
			
	}	
}