package com.grovix.awstickman.factory.stickman;

import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.grovix.awstickman.config.StickmanConst;

public class StickmanPolygonShape {
	public PolygonShape  armPolygonShape, legPolygonShape, bodyPolygonShape;
	public CircleShape headCircleShape;
	
	public StickmanPolygonShape(StickmanConst c){
		
		headCircleShape = new CircleShape();
		headCircleShape.setRadius(c.headRadius);
		
		armPolygonShape = new PolygonShape();
		armPolygonShape.setAsBox(c.armX, c.armY);
		
		legPolygonShape = new PolygonShape();
		legPolygonShape.setAsBox(c.legX, c.legY);
		
		bodyPolygonShape = new PolygonShape();
		bodyPolygonShape.setAsBox(c.bodyX,c.bodyY);
	}
	
	public void dispose(){
		headCircleShape.dispose();
		armPolygonShape.dispose();
		legPolygonShape.dispose();
		bodyPolygonShape.dispose();
	}
}
