package com.grovix.awstickman.factory.stickman;

import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.grovix.awstickman.config.StickmanConst;


public class StickmanFixture {
	public FixtureDef headFixtureDef, armFixtureDef, bodyFixtureDef, legFixtureDef;
	private float stdFriction=.9f;
	private float stdRestitution=.8f;
	public StickmanPolygonShape polygons;
	
	public StickmanFixture(StickmanConst c){
		polygons = new StickmanPolygonShape(c);
		
		headFixtureDef = new FixtureDef();
		armFixtureDef = new FixtureDef();
		bodyFixtureDef = new FixtureDef();
		legFixtureDef = new FixtureDef();
		
		headFixtureDef.shape = polygons.headCircleShape;
		armFixtureDef.shape = polygons.armPolygonShape;
		bodyFixtureDef.shape = polygons.bodyPolygonShape;
		legFixtureDef.shape = polygons.legPolygonShape;
		
		legFixtureDef.filter.groupIndex = -2;
		
		setShapes(headFixtureDef, c.headDensity);
		setShapes(bodyFixtureDef, c.bodyDensity); 
		setShapes(armFixtureDef, c.armDensity);
		setShapes(legFixtureDef, c.legDensity);
		
	}
	
	public void setShapes(FixtureDef innerFixtureDef, float density){
		innerFixtureDef.density=density;
		innerFixtureDef.friction=stdFriction;
		innerFixtureDef.restitution=stdRestitution;
	}
}

