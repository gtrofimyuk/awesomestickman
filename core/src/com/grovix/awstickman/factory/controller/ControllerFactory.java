package com.grovix.awstickman.factory.controller;

import com.grovix.awstickman.cpucontrol.Controller;
import com.grovix.awstickman.cpucontrol.GoodMoving;
import com.grovix.awstickman.cpucontrol.SimpleController;
import com.grovix.awstickman.factory.level.Args;
import com.grovix.awstickman.kernel.HeroPositionGetter;
import com.grovix.awstickman.stickman.StickmanAction;

public class ControllerFactory {
	
	StickmanAction action;
	HeroPositionGetter kernel;
	
	public ControllerFactory(HeroPositionGetter inputKernel ,StickmanAction inputAction){
		action = inputAction;
		kernel = inputKernel;
	}
	
	public Controller get(int type){
		switch(type){
		case Args.ControllerType.simple:
			return new SimpleController(kernel, action);
		case Args.ControllerType.goodMoving:
			return new GoodMoving(kernel, action);
		default: throw new RuntimeException("Error Controller type");
		}
	}

}
