package com.grovix.awstickman.factory.config;

import java.io.IOException;

import com.grovix.awstickman.config.StickmanConst;
import com.grovix.awstickman.factory.level.Args;

public class StickmanConstFactory {
	String path = new String();
	
	public StickmanConstFactory(int type){
		
		switch(type){
		
		case Args.BodyType.standart:
			path = "config/stickman/body/standart.properties";
			break;
			
		case Args.BodyType.doubleSize:
			path = "config/stickman/body/doublesize.properties";
			break;
			
		case Args.BodyType.small:
			path = "config/stickman/body/small.properties";
			break;
			
			default:
				throw new RuntimeException("Error bodyConst type readed");
		}
	}
	
	public StickmanConst get() throws IOException{
		return new StickmanConst(path);
	}
}
