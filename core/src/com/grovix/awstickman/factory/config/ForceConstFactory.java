package com.grovix.awstickman.factory.config;

import java.io.IOException;

import com.grovix.awstickman.config.ForceConst;
import com.grovix.awstickman.factory.level.Args;

public class ForceConstFactory {
String path = new String();
	
	public ForceConstFactory(int type){
		
		switch(type){
		
		case Args.ForceType.hero:
			path = "config/stickman/force/hero.properties";
			break;
			
		case Args.ForceType.standart:
			path = "config/stickman/force/standart.properties";
			break;
			
		case Args.ForceType.veryFast:
			path = "config/stickman/force/veryfast.properties";
			break;
			
		case Args.ForceType.veryStrong:
			path = "config/stickman/force/verystrong.properties";
			break;
			
			default:
				throw new RuntimeException("Error forceConst type readed");
		}
		
	}
	
	public ForceConst get() throws IOException{
		return new ForceConst(path);
	}
}
