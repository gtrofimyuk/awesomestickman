package com.grovix.awstickman.factory.level;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.physics.box2d.World;

public class LevelFactory {
	boolean readFlag = true;
	private World world;
	
	public List<GroundItem> groundList = new ArrayList<>();
	public List<StickmanItem> stickmanList = new ArrayList<>();
	public List<StartEndItem> startEndList = new ArrayList<>();
	
	@SuppressWarnings("unchecked")
	public LevelFactory(String path, World inputWorld) throws RuntimeException{
		
		world = inputWorld;
		
		ItemFactory factory = null;
		
		List<?> list = new ArrayList<>();
		BufferedReader reader = Gdx.files.internal(path).reader(1024);

		
		try {
				while (readFlag) {		
					String buf = reader.readLine();
					if(buf == null){
						readFlag = false;
						break;
					}
					switch(buf){
					case "ground":
						factory = new GroundItemFactory(5);  //TODO: make enum with counts
						list = groundList;
						break;
					case "stickmans":
						factory = new StickmanItemFactory(5);
						list = stickmanList;
						break;
					case "startend":
						factory = new StartEndItemFactory(4);
						list = startEndList;
						break;
						
					default:
						factory.getItem().addToList(buf, (List<Item>) list);
						break;
					}
				}
		}catch (IOException e) {
			e.printStackTrace();
			}
		
		System.out.println("startend");

		for(int i = 0; i < startEndList.get(0).values.length; i++)
			System.out.println(startEndList.get(0).values[i]);
		
		System.out.println("startend");

		
		new GroundFactory(world).addShapes(groundList);

	}
	
	public List<StickmanItem> getStickmans(){
		return stickmanList;
	}
	
	public List<StartEndItem> getStartEnd(){
		return startEndList;
	}
	
	abstract class Item{
		
		protected int[] values;
		protected int argCount;

		public Item(int argCount){
			this.argCount = argCount;
			values = new int[this.argCount];
		}
		
		public void addToList(String str, List<Item> list){
				String[] args = str.split(" ");
				for(int i=0; i < values.length; i++){
					values[i]=Integer.valueOf(args[i]);
				}
				list.add(this);
		}
		public int[] getValues(){
			return values;
		}
	}
	
	class GroundItem extends Item{

		public GroundItem(int argCount) {
			super(argCount);
		}
	}
	
	class StickmanItem extends Item{
		public StickmanItem(int argCount) {
			super(argCount);
		}
	}
	
	public class StartEndItem extends Item{
		public StartEndItem(int argCount) {
			super(argCount);
		}
	}
	
	abstract class ItemFactory{
		int argCount;
		public ItemFactory(int arg){
			this.argCount = arg;
		}
		public abstract Item getItem();
	}
	
	class GroundItemFactory extends ItemFactory{
		public GroundItemFactory(int arg) {
			super(arg);
		}
		public Item getItem() {
			return new GroundItem(argCount);
		}	
	}
	
	class StickmanItemFactory extends ItemFactory{

		public StickmanItemFactory(int arg) {
			super(arg);
		}
		public Item getItem() {
			return new StickmanItem(argCount);
		}
		
	}
	
	class StartEndItemFactory extends ItemFactory{
		
		public StartEndItemFactory(int arg) {
			super(arg);
		}
		
		public Item getItem() {
			return new StartEndItem(argCount);
		}
		
	}
}
