package com.grovix.awstickman.factory.level;

public class Args {
	
	public class GroundType {
		public static final int SimpleGround = 1;
		public static final int ThornGround = 2;
	}
	
	public class GroundArgs {
		public static final int startX = 0;
		public static final int startY = 1;
		public static final int endX = 2;
		public static final int endY = 3;
		public static final int type = 4;
	}
	
	public class StickmanArgs {
		public static final int posX = 0;
		public static final int posY = 1;
		public static final int body = 2;
		public static final int force = 3;
		public static final int controller = 4;
	}
	
	public class BodyType {
		public static final int standart = 0;
		public static final int small = 1;
		public static final int doubleSize = 2;
	}
	
	public class ForceType{
		public static final int hero = 0;
		public static final int standart = 1;
		public static final int veryFast = 2;
		public static final int veryStrong = 3;
	}
	
	public class ControllerType{
		public static final int simple = 0;
		public static final int goodMoving = 1;
	}
	
	public class StartEndArgs{
		public static final int startX = 0;
		public static final int startY = 1;
		public static final int endX = 2;
		public static final int endY = 3;
	}

}
