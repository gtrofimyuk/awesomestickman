package com.grovix.awstickman.factory.level;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.grovix.awstickman.config.WorldConst;
import com.grovix.awstickman.cpucontrol.Controller;
import com.grovix.awstickman.factory.config.ForceConstFactory;
import com.grovix.awstickman.factory.config.StickmanConstFactory;
import com.grovix.awstickman.factory.controller.ControllerFactory;
import com.grovix.awstickman.factory.level.LevelFactory.StickmanItem;
import com.grovix.awstickman.kernel.HeroPositionGetter;
import com.grovix.awstickman.stickman.Stickman;

public class StickmanFactory {
	private World world;
	public WorldConst w = new WorldConst();
	
	private List<Stickman> bufferActor = new ArrayList<>();
    private List<Controller> bufferContr = new ArrayList<>();
    private HeroPositionGetter getter;

	public StickmanFactory(World world, HeroPositionGetter inputGetter){
		this.world = world;
		getter = inputGetter;
	}
	
	public void addStickmans(List<StickmanItem> actors){
		for(int i = 0; i < actors.size(); i++){
			int temp[] = actors.get(i).values;
			Stickman born = null;
			try {
				born = new Stickman(world, new Vector2(temp[Args.StickmanArgs.posX]/1.5f, temp[Args.StickmanArgs.posY]/1.5f),
						new StickmanConstFactory(temp[Args.StickmanArgs.body]).get(),
						new ForceConstFactory(temp[Args.StickmanArgs.force]).get());
			} catch (IOException e) {
				e.printStackTrace();
			}
			
			bufferActor.add(born);
			bufferContr.add(new ControllerFactory(getter, born.action).get(temp[Args.StickmanArgs.controller]));
		}
	}
	
	public List<Stickman> getActorList(){
		return bufferActor;
	}
	
	public List<Controller> getControllerList(){
		return bufferContr;
	}
}
