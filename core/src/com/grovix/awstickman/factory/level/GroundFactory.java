package com.grovix.awstickman.factory.level;

import java.util.List;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.BodyDef.BodyType;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;
import com.grovix.awstickman.config.WorldConst;
import com.grovix.awstickman.factory.level.LevelFactory.GroundItem;


public class GroundFactory {
	private World world;
	public WorldConst w = new WorldConst();

	public GroundFactory(World world){
		this.world = world;
	}
	
	public void addShapes(List<GroundItem> shapes){
		BodyDef bodyDef= new BodyDef();
		FixtureDef fixtureDef = new FixtureDef();
	
		bodyDef.type = BodyType.StaticBody;
		bodyDef.position.set(0,0);
		
		for(int i = 0; i < shapes.size(); i++){
					
			float s = w.shapeWidth;
			int []values = shapes.get(i).values;
			for(int j = 0; j < values.length -1 ; j++){
				values[j]/= 1.5f; //TODO make it const
			}
			float startX = values[Args.GroundArgs.startX];
			float startY = values[Args.GroundArgs.startY];
			float endX = values[Args.GroundArgs.endX];
			float endY = values[Args.GroundArgs.endY];
			
			PolygonShape groundShape = new PolygonShape();
			
			float x = Math.abs(startX - endX);
			float y = Math.abs(startY - endY);
			float length = (float) Math.sqrt(x*x + y*y);
			float posX = (endX + startX)/2;
			float posY = (endY + startY)/2;
			
			groundShape.setAsBox(s, length/2, new Vector2(posX, posY), (float) (Math.asin(y/length)+ Math.PI/2));
			
			fixtureDef.shape = groundShape;
			fixtureDef.friction = .5f;
			fixtureDef.restitution = 0.95f;
			
			Body ground = world.createBody(bodyDef);
			ground.setUserData(values[Args.GroundArgs.type]);
			ground.createFixture(fixtureDef);
			
			
			groundShape.dispose();
		}
	}
}
