package com.grovix.awstickman.screens;

public class ScreenSelector {
	
	private static String[] levels;
	private static int index = 0;
	
	public ScreenSelector(){
		
	}
	
	public void setLevelsArray(String[] inputLevels){
		levels = inputLevels;
		for(int i = 0; i < levels.length; i++){
			levels[i] = new String("levels/"+levels[i]);
		}
	}
	
	public void setIndex(int inputIndex){
		index = inputIndex;
	}
	
	public int getIndex(){
		return index;
	}
	
	public String[] getLevelList(){
		return levels;
	}

}
