package com.grovix.awstickman.screens;

import java.io.File;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.scenes.scene2d.InputEvent;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.List;
import com.badlogic.gdx.scenes.scene2d.ui.ScrollPane;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.TextButton;
import com.badlogic.gdx.scenes.scene2d.utils.ClickListener;
import com.grovix.awstickman.Level;

public class LevelMenu implements Screen {
	
	private Stage stage;
	private Table table, scrollTable;
	private Skin skin;
	private ScrollPane scrollPane;
	private TextButton buttonPlay, buttonBack;
	
	private void setupTable(){
		table.clear();
		//table.setBounds(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
		table.setBounds(0, 0, stage.getWidth(), stage.getHeight());
		table.add().width(table.getWidth() / 3);
		table.add(new Label("Select Level",skin)).width(table.getWidth() / 3);
		table.add().width(table.getWidth() / 3).row();
		table.add(scrollPane).left().expandY();
		table.add(buttonPlay).size(300*stage.getWidth()/1280, 150*stage.getHeight()/720);
		table.add(buttonBack).bottom().right();
		
	}
	
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		//Table.drawDebug(stage);
		stage.act(delta);
		stage.draw();
		
	}

	@Override
	public void resize(int width, int height) {
		//stage.setViewport(width, height, true); TODO: change this
		table.setClip(true);
		setupTable();
	}

	@Override
	public void show() {
		//Texture.setEnforcePotImages(false); TODO: look at this
		stage = new Stage();
		
		Gdx.input.setInputProcessor(stage);
		

		skin = new Skin(Gdx.files.internal("ui/levelMenuSkin.json"), new TextureAtlas(Gdx.files.internal("ui/atlas.pack")));
		
		table = new Table(skin);
		table.setFillParent(true);
		
		
		//TODO - ������� ���������� ��������, � ���� �������))
		
		
		
		File path =  Gdx.files.internal("levels").file();
		final String [] fileList = {new String("level1_Intro"), new String("level2_Apperetive")};
		final List list = new List(skin);
		list.setItems(fileList);
		
//        scrollTable  = new Table(skin);
//        scrollTable.add("1");
//        scrollTable.row();
//        scrollTable.add("2");
//        scrollTable.row();
//        scrollTable.add("3");

        //this.stage.addActor(table);
		//TODO: ��� ������ ���� ���?
		scrollPane = new ScrollPane(list, skin);
        //table.add(scrollPane).fill().expand();

		//scrollPane = new ScrollPane(widget, skin)	
		buttonPlay = new TextButton("PLAY", skin);
		buttonPlay.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				int index = list.getSelectedIndex();
				((Game) Gdx.app.getApplicationListener()).setScreen(new Level("levels/"+fileList[index]));
			}
		});
		buttonPlay.pad(15);
		
		buttonBack = new TextButton("back", skin);
		buttonBack.addListener(new ClickListener(){
			@Override
			public void clicked(InputEvent event, float x, float y) {
				((Game) Gdx.app.getApplicationListener()).setScreen(new MainMenu());
			}
		}
		);
		buttonBack.pad(10);
		
		setupTable();
		
		//putting stuff together

		stage.addActor(table);
	}

	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {
		stage.dispose();
		skin.dispose();
	}

}
