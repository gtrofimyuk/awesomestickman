package com.grovix.awstickman.kernel;

import java.util.Map;
import java.util.TreeMap;

import com.grovix.awstickman.stickman.Stickman;

public class IdKeeper {
	private Map<Integer, Stickman> stickmanMap = new TreeMap<>();

	public IdKeeper(){
		
	}
	
	public void addStickman(Stickman inputStickman){
		stickmanMap.put(inputStickman.getId(), inputStickman);
	}
	 public Map<Integer, Stickman> getMap(){
		 return stickmanMap;
	 }
	 
	 public void clear(){
		 stickmanMap.clear();
	 }
}
