package com.grovix.awstickman.kernel;

import com.badlogic.gdx.math.Vector2;
import com.grovix.awstickman.stickman.Stickman;

public class HeroPositionGetter {
	private Stickman stickman;
	public HeroPositionGetter(Stickman hero){
		stickman = hero;
	}
	
	public Vector2 getHeroPosition(){
		return stickman.getPosition();
	}
}
