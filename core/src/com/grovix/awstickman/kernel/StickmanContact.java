package com.grovix.awstickman.kernel;

import java.util.Map;
import java.util.TreeMap;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.grovix.awstickman.config.ForceConst;
import com.grovix.awstickman.config.WorldConst;
import com.grovix.awstickman.factory.level.Args;
import com.grovix.awstickman.stickman.Stickman;


public class StickmanContact implements ContactListener {
	
	private Map<Integer, Stickman> stickmanMap = new TreeMap<>();
	private static int count =  1;
	
	public WorldConst w = new WorldConst();
	public ForceConst fA, fB;
	
	public StickmanContact (IdKeeper inputIdKeeper){
		stickmanMap = inputIdKeeper.getMap();
		if(count > 1)
			throw new RuntimeException("Created second StickmanContact");
		count ++;
	}
	
	public void contactSolver(Contact contact, ContactImpulse impulse){
	int idA = getStickmanId((Integer) contact.getFixtureA().getBody().getUserData());
	int idB = getStickmanId((Integer) contact.getFixtureB().getBody().getUserData());
		if(idA == 10 | idB == 10){
		fA = stickmanMap.get(idA).force;
		fB = stickmanMap.get(idB).force;
		stickmanMap.get(idA).body.torso.setAngularDamping(0);
		stickmanMap.get(idB).body.torso.setAngularDamping(0);

		if(stickmanMap.get(idA).action.isKick()){
			stickmanMap.get(idB).state.throwBody
			(new Vector2(stickmanMap.get(idA).action.getDirection().x*fA.throwForce,
			 stickmanMap.get(idA).action.getDirection().y*fA.throwForce), fA.throwTimer);
			stickmanMap.get(idB).state.decreaseHealth(fA.kickDamage);
			stickmanMap.get(idA).action.stopKick();

		}
		if(stickmanMap.get(idB).action.isKick()){
			stickmanMap.get(idA).state.throwBody
			(new Vector2(stickmanMap.get(idB).action.getDirection().x*fB.throwForce,
			 stickmanMap.get(idB).action.getDirection().y*fB.throwForce), fB.throwTimer);
			stickmanMap.get(idA).state.decreaseHealth(fB.kickDamage);
			stickmanMap.get(idB).action.stopKick();
		}
		
		if(stickmanMap.get(idA).action.isArmKick){
			stickmanMap.get(idB).state.decreaseHealth(fA.kickDamage/2);
			stickmanMap.get(idB).state.throwBody
			(new Vector2(stickmanMap.get(idA).action.getDirection().x*fA.throwForce/1.5f,
			 stickmanMap.get(idA).action.getDirection().y*fA.throwForce/1.5f), fA.throwTimer/8);
			stickmanMap.get(idA).action.isArmKick = false;
		}
		if(stickmanMap.get(idB).action.isArmKick){
			stickmanMap.get(idA).state.decreaseHealth(fA.kickDamage/2);
			stickmanMap.get(idA).state.throwBody
			(new Vector2(stickmanMap.get(idB).action.getDirection().x*fB.throwForce/1.5f,
			 stickmanMap.get(idB).action.getDirection().y*fB.throwForce/1.5f), fB.throwTimer/8);
			stickmanMap.get(idB).action.isArmKick = false;
		}
		}
	}
	
	//���� ��������� ����� ��� �������� � �������� ������ ��� 110 ������
	public void setDamage(int idA, int idB){
		
		float velocityA = getBodyVelocity(idA);
		float velocityB = getBodyVelocity(idB);
		
		float difference = Math.abs(velocityA - velocityB);
		
		if(difference > 30 ){
			if(velocityA >= velocityB){
				stickmanMap.get(idB).state.decreaseHealth(0.01f*difference);//TODO: make it config
			}
			else
				stickmanMap.get(idA).state.decreaseHealth(0.01f*difference);
		}
	}
	
	@Override
	public void beginContact(Contact contact) {

	}

	@Override
	public void endContact(Contact contact) {

	}

	@Override
	public void preSolve(Contact contact, Manifold oldManifold) {

	}

	@Override
	//id of hero == 10
	public void postSolve(Contact contact, ContactImpulse impulse) {
		if(!contact.getFixtureA().getBody().getUserData().equals(Args.GroundType.SimpleGround) &&
				!contact.getFixtureB().getBody().getUserData().equals(Args.GroundType.SimpleGround) ){
			if(contact.getFixtureA().getBody().getUserData().equals(Args.GroundType.ThornGround)){
				int id = getStickmanId((Integer)contact.getFixtureB().getBody().getUserData());
				stickmanMap.get(id).state.decreaseHealth(w.thornDamage);
				stickmanMap.get(id).body.torso.setAngularDamping(0);
			}
			else if(contact.getFixtureB().getBody().getUserData().equals(Args.GroundType.ThornGround)){
				int id = getStickmanId((Integer)contact.getFixtureA().getBody().getUserData());
				stickmanMap.get(id).state.decreaseHealth(w.thornDamage);
				stickmanMap.get(id).body.torso.setAngularDamping(0);
			}
			else {
				contactSolver(contact, impulse);
			}
		}
	}
	
	private int getStickmanId(int id){
		int temp = id - id %10;
		return temp;
	}

	
	private float getBodyVelocity(int id){
		return stickmanMap.get(id).getVelocity();
	}
	
	public void dispose(){
		count = 1;
	}
	
}
