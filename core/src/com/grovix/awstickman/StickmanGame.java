package com.grovix.awstickman;

import com.badlogic.gdx.Game;
import com.grovix.awstickman.screens.Splash;

public class StickmanGame extends Game {
public static final String TITLE = "AwesomeStickman", VERSION = "0.7.5.0";
	
	@Override
	public void create() {		
		setScreen(new Splash());
		//setScreen(new Level("levels/level2_Apperetive"));
	}

	@Override
	public void dispose() {
		super.dispose();
	}

	@Override
	public void render() {	
		super.render();
	}
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
	}

	@Override
	public void pause() {
		super.pause();
	}

	@Override
	public void resume() {
		super.resume();
	}
}
