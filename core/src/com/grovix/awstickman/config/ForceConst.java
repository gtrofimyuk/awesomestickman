package com.grovix.awstickman.config;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.TreeMap;

import com.badlogic.gdx.Gdx;

public class ForceConst {
	public float moveForce = 10;
	public float legForce = 50000;
	public float deceleration = 0.75f;
	public float throwForce = 500000f;
	public float throwTimer = 100f;
	public float kickDamage = 70f;
	
	public ForceConst(){
		
	}
	
	public void load(String path) throws IOException{
		//Properties prop = new Properties();
		TreeMap<String, Float> treemap = new TreeMap<String, Float>();
		boolean readFlag = true;
		BufferedReader reader = Gdx.files.internal(path).reader(1024);
		try {
			while (readFlag) {		
				String buf = reader.readLine();
				if(buf == null){
					readFlag = false;
					break;
				}
				
				String[] tmp = buf.split("=");
				treemap.put(tmp[0], Float.valueOf(tmp[1]));
			}
		}catch (IOException e) {
				e.printStackTrace();
		}
				
		moveForce = treemap.get("moveForce");
		legForce = treemap.get("legForce");
		deceleration = treemap.get("deceleration");
		throwForce = treemap.get("throwForce");
		throwTimer = treemap.get("throwTimer");
		kickDamage = treemap.get("kickDamage");
		
		reader.close();
	}
	
	public ForceConst(ForceConst inputForce){
		this.moveForce = inputForce.moveForce;
		this.legForce = inputForce.legForce;
		this.deceleration = inputForce.deceleration;
		this.throwForce = inputForce.throwForce;
		this.throwTimer = inputForce.throwTimer;
		this.kickDamage = inputForce.kickDamage;
	}
	
	public ForceConst(String path) throws IOException{
		this.load(path);
	}
}
