package com.grovix.awstickman.config;

import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.TreeMap;

import com.badlogic.gdx.Gdx;

public class StickmanConst {
	public float headRadius = .45f, 
			armX = .2f, armY = .9f, 
			legX = .2f, legY= .9f,
			bodyX=.2f, bodyY = 1.35f,
			armDensity = 3.5f,
			headDensity = 10f,
			bodyDensity = 25f,
			legDensity = 10f,
			joinTorso = 0.9f,
			health = 200;
	
	public StickmanConst(){
	}
	
	public void load(String path) throws IOException{
		TreeMap<String, Float> treemap = new TreeMap<String, Float>();
		boolean readFlag = true;
		BufferedReader reader = Gdx.files.internal(path).reader(1024);
		try {
			while (readFlag) {		
				String buf = reader.readLine();
				if(buf == null){
					readFlag = false;
					break;
				}
				
				String[] tmp = buf.split("=");
				treemap.put(tmp[0], Float.valueOf(tmp[1]));
			}
		}catch (IOException e) {
				e.printStackTrace();
		}
		
		headRadius = treemap.get("headRadius");
		armX = treemap.get("armX");
		armY = treemap.get("armY");
		legX = treemap.get("legX");
		legY = treemap.get("legY");
		bodyX = treemap.get("bodyX");
		bodyY = treemap.get("bodyY");
		armDensity = treemap.get("armDensity");
		legDensity = treemap.get("legDensity");
		bodyDensity = treemap.get("bodyDensity");
		headDensity = treemap.get("headDensity");
		joinTorso = treemap.get("joinTorso");
		health = treemap.get("health");
		
		reader.close();

	}
	
	public StickmanConst(String path) throws IOException{
		this.load(path);
	}
}
