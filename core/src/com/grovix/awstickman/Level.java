package com.grovix.awstickman;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.grovix.awstickman.cpucontrol.Controller;
import com.grovix.awstickman.factory.config.ForceConstFactory;
import com.grovix.awstickman.factory.config.StickmanConstFactory;
import com.grovix.awstickman.factory.level.Args;
import com.grovix.awstickman.factory.level.LevelFactory;
import com.grovix.awstickman.factory.level.StickmanFactory;
import com.grovix.awstickman.factory.stickman.StickmanBody;
import com.grovix.awstickman.kernel.HeroPositionGetter;
import com.grovix.awstickman.kernel.IdKeeper;
import com.grovix.awstickman.kernel.StickmanContact;
import com.grovix.awstickman.screens.LevelMenu;
import com.grovix.awstickman.stickman.KeyboardController;
import com.grovix.awstickman.stickman.Stickman;

public class Level implements Screen {
	
	private World world;
	private Box2DDebugRenderer debugRenderer;
	private OrthographicCamera camera;
	private Sprite splash;
	private Sprite death;
	private Sprite win;
	private SpriteBatch batch;
	private BitmapFont font;
	
    private float g=-9.81f;
    private float CAM_SCALE = 20;
    private float TIMESTEP = 1 / 120f;
    private int VELOCITYITERATIONS = 8, POSITIONITERATION = 3;//it's from Box2D example, it's ok =)

    private String path;
    private Vector2 finish; //TODO make finish symbol - simple box
    
    Stickman hero;
    List<Stickman> actor = new LinkedList<>();
    List<Stickman> bufferActor = new ArrayList<>();// make lazy init - depends on camera.zoom
    List<Controller> contr;
    List<Controller> bufferContr = new ArrayList<>();
    //for i'th stickman we add i'th controller! (it's fucking architecture!!!)
    
    StickmanContact contactRuler;
    IdKeeper idKeeper;
    int deathTimer = 100;
    int winTimer = 100;
    private boolean winFlag = false;
        
    public Level(String inputPath){
    	path = inputPath;
    }
    
    private double detectionRange = 50;
    
	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		
		detectionRange = Gdx.graphics.getWidth()/ CAM_SCALE * camera.zoom/1.5f; //depends on levelFactory by magic const

		world.step(TIMESTEP, VELOCITYITERATIONS, POSITIONITERATION);
				
		//lazy init
		for(int i = 0; i < bufferActor.size(); i++){
			Vector2 bufCoord = bufferActor.get(i).getPosition();
			Vector2 heroPos = hero.getPosition();
			if(Math.sqrt(Math.pow(heroPos.x - bufCoord.x, 2)+ Math.pow(heroPos.y - bufCoord.y, 2)) <= detectionRange){
				bufferActor.get(i).action.setActive(true);
				actor.add(bufferActor.get(i));
				contr.add(bufferContr.get(i));
				bufferActor.remove(i);
				bufferContr.remove(i);
			}
		}

		for(int i = 0; i < actor.size(); i++){
			if(actor.get(i).isLive){
				actor.get(i).update();
				contr.get(i).update();
			}
			else{
				hero.state.increaseHealth(0.1f); //TODO: make it constant
				actor.get(i).deathTimer --;
				actor.get(i).update();
				if(actor.get(i).deathTimer == 0){
					contr.remove(i);
					actor.remove(i);
				}
			}
		}

		camera.position.set(hero.getPosition().x, hero.getPosition().y, 0);
		camera.update();
		
		//draw text info
		String str = new String("Health: " + hero.state.getHealth());
		
		batch.begin();
		batch.setProjectionMatrix(camera.projection.scale(0.025f*camera.zoom, 0.025f*camera.zoom, 1));
		splash.draw(batch);
		font.draw(batch, str, -1200, 700);
		
		
		if(hero.isLive){
			hero.update();
			hero.state.increaseHealth(0.1f); //TODO make it const
		}	
		else {
			death.draw(batch);
		}
		
		if(winFlag){
			win.draw(batch);
			winTimer--;
		}
		
		batch.end();
		debugRenderer.render(world, camera.combined);
		
		if(hero.getPosition().x >= finish.x & hero.getPosition().y > finish.y & actor.size() == 0){
			winFlag = true;
		}
		
		if(winTimer == 0)
			((Game) Gdx.app.getApplicationListener()).setScreen(new LevelMenu()); //Make win screen
		
		if(!hero.isLive){
			if(deathTimer > 0)
				deathTimer--;
			else{
				((Game) Gdx.app.getApplicationListener()).setScreen(new Level(path));
			}
		}
	}

	@Override
	public void resize(int width, int height) {
		camera.viewportWidth = width / CAM_SCALE;
		camera.viewportHeight = height / CAM_SCALE;
		splash.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		splash.setPosition(-Gdx.graphics.getWidth(), -Gdx.graphics.getHeight());
		death.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		death.setPosition(-Gdx.graphics.getWidth(), -Gdx.graphics.getHeight());
		win.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		win.setPosition(-Gdx.graphics.getWidth(), -Gdx.graphics.getHeight());

	}

	@Override
	public void show() {
		//Texture.setEnforcePotImages(false);
		world = new World(new Vector2(0,  g), true);
		debugRenderer = new Box2DDebugRenderer();
		
		camera = new OrthographicCamera();
		camera.zoom += 0.3f;
		
		batch = new SpriteBatch();
		
		font = new BitmapFont(Gdx.files.internal("font/white_64.fnt"));
		font.setColor(Color.WHITE);
		font.setScale(1f);
		
		Texture splashTexture = new Texture("img/kosmos.png");
		splash = new Sprite(splashTexture);
		splash.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		splash.setPosition(-Gdx.graphics.getWidth(), -Gdx.graphics.getHeight());
		
		Texture deathTexture = new Texture("img/death.png");
		death = new Sprite(deathTexture);
		death.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		death.setPosition(-Gdx.graphics.getWidth(), -Gdx.graphics.getHeight());
		
		Texture winTexture = new Texture("img/win.png");
		win = new Sprite(winTexture);
		win.setSize(Gdx.graphics.getWidth()*2, Gdx.graphics.getHeight()*2);
		win.setPosition(-Gdx.graphics.getWidth(), -Gdx.graphics.getHeight());
		
		contr = new LinkedList<>();
		LevelFactory level = new LevelFactory(path, world);
		
		int[] values = level.getStartEnd().get(0).getValues(); //TODO Maybe clear?
		
		for(int i = 0; i < values.length; i++)
		System.out.println(values[i]);
		
		Vector2 start = new Vector2(values[Args.StartEndArgs.startX]/1.5f,
									values[Args.StartEndArgs.startY]/1.5f);
		finish = new Vector2(values[Args.StartEndArgs.endX]/1.5f, values[Args.StartEndArgs.endY]/1.5f);
		
		try {
			hero = new Stickman(world, start,new StickmanConstFactory(Args.BodyType.standart).get(), 
					new ForceConstFactory(Args.ForceType.hero).get());
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		StickmanFactory factory = new StickmanFactory(world, new HeroPositionGetter(hero));
		factory.addStickmans(level.getStickmans());
		bufferActor = factory.getActorList();
		bufferContr = factory.getControllerList();
		
		for(int i = 0; i < bufferActor.size(); i++)
			bufferActor.get(i).action.setActive(false);

		setKeys();
		
		idKeeper = new IdKeeper();
		idKeeper.addStickman(hero);
		
		for(int i = 0; i < bufferActor.size(); i++)
			idKeeper.addStickman(bufferActor.get(i));
		
		contactRuler = new StickmanContact(idKeeper);
		world.setContactListener(contactRuler);
		
	}
	
	@Override
	public void hide() {
		dispose();
	}

	@Override
	public void pause() {

	}

	@Override
	public void resume() {

	}

	@Override
	public void dispose() {	
		debugRenderer.dispose();
		contactRuler.dispose();
		actor.clear();
		contr.clear();
		bufferActor.clear();
		bufferContr.clear();
		idKeeper.clear();
		world.dispose();
		StickmanBody.id = 10;
	}
	
	public void setKeys(){
		KeyboardController keyControl = new KeyboardController(hero.action);
		
		//Gdx.input.setInputProcessor(keyControl);

		Gdx.input.setInputProcessor(new InputMultiplexer(new InputController(){
			@Override
			public boolean scrolled(int amount) {
				camera.zoom += amount / 25f;
				return true;
			};
			@Override

			public boolean keyDown(int keycode) {
				// it's so failure code, but i need to test some things
				switch(keycode){
				case Keys.ESCAPE:
					contactRuler.dispose();
					((Game) Gdx.app.getApplicationListener()).setScreen(new LevelMenu());
				break;
				}
				return false;
			}
		
		},keyControl));
	}
}